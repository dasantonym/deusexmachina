//
//  ArduinoLedControl.h
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "ofMain.h"
#include "ArduinoFirmataDevice.h"

class ArduinoLedControl
{
public:
    void init();
    void update();
    void addArduino(std::string serialPath, int baudRate);
    std::vector<ArduinoFirmataDevice> arduinoDevices;
    
private:
    
};
