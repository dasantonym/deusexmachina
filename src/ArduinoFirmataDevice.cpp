//
//  ArduinoFirmataDevice.cpp
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "ArduinoFirmataDevice.h"

void ArduinoFirmataDevice::open(std::string serialPath, int baudRate)
{
    _device.connect(serialPath, baudRate);
    ofAddListener(_device.EInitialized, this, &ArduinoFirmataDevice::setupArduino);
}

void ArduinoFirmataDevice::update()
{
    if (_serial.isInitialized())
    {
        _device.update();
    }
}

void ArduinoFirmataDevice::setupArduino(const int &version)
{
    ofRemoveListener(_device.EInitialized, this, &ArduinoFirmataDevice::setupArduino);

    _isReady = true;
    
    // print firmware name and version to the console
    ofLogNotice("ArduinoFirmataDevice::setupArduino") << _device.getFirmwareName();
    ofLogNotice("ArduinoFirmataDevice::setupArduino") << "firmata v" << _device.getMajorFirmwareVersion() << "." << _device.getMinorFirmwareVersion();
    
    // Note: pins A0 - A5 can be used as digital input and output.
    // Refer to them as pins 14 - 19 if using StandardFirmata from Arduino 1.0.
    // If using Arduino 0022 or older, then use 16 - 21.
    // Firmata pin numbering changed in version 2.3 (which is included in Arduino 1.0)
    
    _device.sendDigitalPinMode(3, ARD_PWM);
    _device.sendDigitalPinMode(5, ARD_PWM);
    _device.sendDigitalPinMode(6, ARD_PWM);
    _device.sendDigitalPinMode(9, ARD_PWM);
    _device.sendDigitalPinMode(10, ARD_PWM);
}

void ArduinoFirmataDevice::setAnalogPinOut(int pin, int value)
{
    if (_isReady) {
        _device.sendPwm(pin, value);
    }
}