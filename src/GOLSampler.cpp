//
//  GOLSampler.cpp
//  DeusExMachina
//
//  Created by Anton on 16/11/14.
//
//

#include "GOLSampler.h"

void GOLSampler::init(int samplesX, int samplesY, GOLGrid* grid)
{
    _samplesX = samplesX;
    _samplesY = samplesY;
    _grid = grid;
    
    for (int n = 0; n < _samplesX*_samplesY; n += 1)
    {
        sampleGrid.push_back(0);
    }
}

void GOLSampler::update()
{
    int sampleCount = 0;
    int ySample = 0;
    int xSample = 0;
    
    for (int n = 0; n < _samplesX*_samplesY; n += 1)
    {
        sampleGrid[n] = 0;
    }
    
    for (int x=0; x<_grid->cols; x++)
    {
        for (int y=0; y<_grid->rows; y++)
        {
            GOLCell *thisCell = &_grid->grid[x][y];
            if (thisCell->currState == true)
            {
                int sampleIndex = floor(double(x)/_grid->cols*_samplesX) * _samplesY + floor(double(y)/_grid->rows*_samplesY);
                sampleGrid[sampleIndex] += 1;
            }
        }
    }
}