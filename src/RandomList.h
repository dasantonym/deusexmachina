//
//  RandomList.h
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include <stdio.h>
#include <vector>

class RandomList
{
public:
    void generate(int size);
    float getValue();
    
private:
    std::vector<float> _values;
    int _size;
    int _pointer;
};