//
//  RelayOscSender.cpp
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "RelayControl.h"
#include "OscDataSender.h"

void RelayControl::init(OscDataSender* oscSender, std::string host, int port, int relayIndex)
{
    ofLogNotice("RelayOscSender::init") << "Adding relay for host " << host << " on port " << ofToString(port);
    _host = host;
    _port = port;
    _oscSender = oscSender;
    index = relayIndex;
}

void RelayControl::setRelayState(int state)
{
    if (state != _relayState)
    {
        _relayState = state;
        vector<float> args;
        args.push_back(float(state));
        _oscSender->sendMessage(_host, _port, "/relay", args);
    }
}

int RelayControl::getRelayState()
{
    return _relayState;
}