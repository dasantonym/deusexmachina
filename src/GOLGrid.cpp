//
//  conway.cpp
//  DeusExMachina
//
//  Created by Anton on 05/11/14.
//
//

#include "GOLGrid.h"
#include "GOLPatterns.h"

void GOLGrid::init(int width, int height, int cellSize, int xOffset, int yOffset, int tickInterval)
{
    _active = false;
    _width = width;
    _height = height;
    _cellSize = cellSize;
    _tickInterval = tickInterval;
    
    cols = _width/_cellSize;
    rows = _height/_cellSize;
    
    _xOffset = xOffset;
    _yOffset = yOffset;

    if (_width % _cellSize != 0 || (_height & _cellSize) != 0)
    {
        float ratio = _width/_height;
        _cellWidth = _cellSize * ratio;
        _cellHeight = _cellSize;
    }
    else
    {
        _cellWidth = _cellSize;
        _cellHeight = _cellSize;
    }

    clear();
    
    _active = true;
}

void GOLGrid::update()
{
    if (ofGetFrameNum() % _tickInterval == 0 && _active)
    {
        tick();
    }
}

void GOLGrid::tick()
{
    // get active neighbors for each cell
    for (int i=0; i<cols; i++)
    {
        for (int j=0; j<rows; j++)
        {
            GOLCell *thisCell = &grid[i][j];
            thisCell->activeNeighbors = getNumActiveNeighbors(i, j);
            bool currState = thisCell->currState;
            int activeNeighbors = thisCell->activeNeighbors;
            
            if (thisCell->ticks > 10 && thisCell->nextState == true)
            {
                
                thisCell->currState = false;
                thisCell->nextState = false;
                
            }
            else
            {

                if (currState == true && activeNeighbors < 2)
                {
                    thisCell->nextState = false;
                    thisCell->ticks = thisCell->ticks + 1;
                }
                else if (currState == true && activeNeighbors > 3)
                {
                    thisCell->nextState = false;
                    thisCell->ticks = thisCell->ticks + 1;
                }
                else if (currState == true && activeNeighbors > 1 && activeNeighbors < 4)
                {
                    thisCell->nextState = true;
                    thisCell->ticks = thisCell->ticks + 1;
                }
                else if (currState == false && activeNeighbors == 3)
                {
                    thisCell->nextState = true;
                    thisCell->ticks = thisCell->ticks + 1;
                }
                
            }
        }
    }
    makeNextStateCurrent();
}

void GOLGrid::makeNextStateCurrent()
{
    for (int i=0; i<cols; i++)
    {
        for (int j=0; j<rows; j++)
        {
            if (grid[i][j].currState == false && grid[i][j].nextState == true)
            {
                grid[i][j].ticks = 0;
            }
            grid[i][j].currState = grid[i][j].nextState;
        }
    }
}

void GOLGrid::draw()
{
    ofNoFill();
    ofRect(_xOffset, _yOffset, _width, _height);
    for (int i=0; i<cols; i++)
    {
        for (int j=0; j<rows; j++)
        {
            GOLCell thisCell = grid[i][j];
            if (thisCell.currState == true)
            {
                ofSetColor(255, 255, 255);
                ofFill();
                ofRect(_xOffset+i*_cellWidth, _yOffset+j*_cellHeight, _cellWidth, _cellHeight);
                ofNoFill();
            }
        }
    }
}

void GOLGrid::clear()
{
    grid = new GOLCell *[cols];
    for (int i=0; i<cols; i++)
    {
        grid[i] = new GOLCell[rows];
        for (int j=0; j<rows; j++)
        {
            GOLCell *thisCell = &grid[i][j];
            thisCell->currState = false;
            thisCell->nextState = false;
            thisCell->ticks = 0;
        }
    }
}

/**
 * Ensure it is a valid col/row combo (on grid) and
 * that this cell's currState is true
 */
int GOLGrid::currState(int col, int row)
{
    return (col >= 0 && row >= 0 &&
            col < cols && row < rows &&
            grid[col][row].currState == true) ? 1 : 0;
}

/**
 * Checks for the number of neighbors that are in an active state
 */
int GOLGrid::getNumActiveNeighbors(int colIndex, int rowIndex)
{
    int ret = 0;

    int prevCol = colIndex-1;
    int nextCol = colIndex+1;
    int prevRow = rowIndex-1;
    int nextRow = rowIndex+1;

    ret += currState(prevCol, prevRow);
    ret += currState(prevCol, rowIndex);
    ret += currState(prevCol, nextRow);

    ret += currState(colIndex, prevRow);
    ret += currState(colIndex, nextRow);

    ret += currState(nextCol, prevRow);
    ret += currState(nextCol, rowIndex);
    ret += currState(nextCol, nextRow);

    return ret;
}

void GOLGrid::addGliderGun(int x, int y)
{
    GOLPatterns::gliderGun(grid, x, y);
}

void GOLGrid::addPufferTrain(int x, int y)
{
    GOLPatterns::pufferTrain(grid, x, y);
}
