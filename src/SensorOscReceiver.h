//
//  SensorOscReceiver.h
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "ofMain.h"
#include "ofxOsc.h"

#define OSC_INPUT_PORT 8888

struct Sensor
{
    float currentValue;
    float lastValue;
};

class SensorOscReceiver
{
public:
    void init(int numSensors);
    void update();
    
    bool isSensorValueChanged(int sensorIndex);
    float getSensorValue(int sensorIndex);
    
    vector<Sensor> sensors;
    
private:
    int _numSensors;
    ofxOscReceiver _oscReciever;
};