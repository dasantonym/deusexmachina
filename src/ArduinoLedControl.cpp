//
//  ArduinoLedControl.cpp
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "ArduinoLedControl.h"

void ArduinoLedControl::init()
{
    arduinoDevices.clear();
}

void ArduinoLedControl::update()
{
    for (int i = 0; i < arduinoDevices.size(); i += 1)
    {
        arduinoDevices[i].update();
    }
}

void ArduinoLedControl::addArduino(std::string serialPath, int baudRate)
{
    ArduinoFirmataDevice arduino;
    arduino.open(serialPath, baudRate);
    arduinoDevices.push_back(arduino);
}
