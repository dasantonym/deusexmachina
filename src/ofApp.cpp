#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetFrameRate(60);
    
    _useCA = true;
    _useRandom = false;
    _useLowPad = false;
    _randomScale = 0.15;
    _lowPadPercent = 1;
    _highPassPercent = 1;
    _useHighPass = false;
    _sampleX = 2;
    _sampleY = 7;
    _sampleTicks = 10;
    _golTicks = 3;
    _numSensors = 14;
    
    rotator = 0;
    
    setupMainGUI();

    randomList.generate(RANDOM_LIST_LENGTH);
    
    gol.init(700, 700, 10, 30, 30, _golTicks);
    golSampler.init(_sampleX, _sampleY, &gol);
    
    sensorReceiver.init(_numSensors);
    
    relayManager.init(dataSender);
    relayManager.addRelay("192.168.100.50", 8000, 1);
    relayManager.addRelay("192.168.100.51", 8000, 2);
    
    ledControl.init();
    
#ifdef linux
    ledControl.addArduino("/dev/ttyACM0", 57600);
    ledControl.addArduino("/dev/ttyUSB0", 57600);
#endif
#ifdef __APPLE__
    ledControl.addArduino("/dev/tty.usbmodem1411", 57600);
    ledControl.addArduino("/dev/tty.usbmodem1421", 57600);
#endif
    
    dmxControl.init();

}

//--------------------------------------------------------------
void ofApp::update()
{
    ledControl.update();
    sensorReceiver.update();
    
    for (int s = 0; s < sensorReceiver.sensors.size(); s += 1)
    {
        if (sensorReceiver.isSensorValueChanged(s) && sensorReceiver.getSensorValue(s) > 0.0)
        {
            int xOffset = 0;
            if (s-6 >= 0)
            {
                xOffset = 1;
            }
            gol.addPufferTrain(xOffset*35+7, int(70/7*(s-xOffset*7)+1));
        }
    }
    
    gol.update();
    golSampler.update();

    int dmxOffset = 0;
    int dmxChannelFix = 0;
    int sampleSize = golSampler.sampleGrid.size();
    vector<float> args;
    for (int i = 0; i < sampleSize; i += 1)
    {
        double brightness = 0.0;
        if (_useCA)
        {
            brightness = double(golSampler.sampleGrid[i])/50.0;
            if (brightness > 1.0)
            {
                brightness = 1.0;
            }
        }
        else
        {
            brightness = double(rotator) / 255.0 + (1.0 / sampleSize) * double(i);
            if (brightness > 1.0) {
                brightness = brightness - 1.0;
            }
        }
        
        int state = brightness > 0.0 ? 1 : 0;
        if (i == 0)
        {
            relayManager.setRelayByIndex(1, state);
        }
        else if (i == 7)
        {
            relayManager.setRelayByIndex(2, state);
        }
        
        if (_useRandom)
        {
            brightness += randomList.getValue()*_randomScale-0.5*_randomScale;
        }
        
        if (_useHighPass && brightness < float(_highPassPercent)/255.0)
        {
            brightness = 0.0;
        }
        
        if (_useLowPad)
        {
            if (brightness < float(_lowPadPercent)/255.0)
            {
                brightness = float(_lowPadPercent)/255.0;
            }
        }
        else
        {
            if (brightness < 0.0)
            {
                brightness = 0.0;
            }
        }
        if (i == 3)
        {
            dmxChannelFix += 1;
        }
        
        args.push_back(brightness);
        
        dmxControl.setChannel(dmxOffset + i + dmxChannelFix, int(255*brightness));

        int brightnessLed = 255-int(brightness*255.0);

        switch (i)
        {
            case 0:
                ledControl.arduinoDevices[0].setAnalogPinOut(3, brightnessLed);
                break;
            case 2:
                ledControl.arduinoDevices[0].setAnalogPinOut(5, brightnessLed);
                break;
            case 4:
                ledControl.arduinoDevices[0].setAnalogPinOut(6, brightnessLed);
                break;
            case 6:
                ledControl.arduinoDevices[1].setAnalogPinOut(10, brightnessLed);
                break;
            case 7:
                ledControl.arduinoDevices[1].setAnalogPinOut(3, brightnessLed);
                break;
            case 9:
                ledControl.arduinoDevices[1].setAnalogPinOut(5, brightnessLed);
                break;
            case 11:
                ledControl.arduinoDevices[1].setAnalogPinOut(6, brightnessLed);
                break;
            case 13:
                ledControl.arduinoDevices[1].setAnalogPinOut(9, brightnessLed);
                break;
            default:
                break;
        }
        
        if (ofGetFrameNum() % 3 == 0)
        {
            rotator += 1;
            if (rotator > 255)
            {
                rotator = 0;
            }
        }
        
    }
    if (ofGetFrameNum() % _sampleTicks == 0) {
        dataSender.sendMessage(OSC_SAMPLE_OUT_IP, OSC_SAMPLE_OUT_PORT, "/sample", args);
    }
    
    dmxControl.sendDmxData();
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofBackground(0, 0, 0);
    ofSetColor(255, 255, 255);
    ofDrawBitmapString("VERKEHRSLEITSTELLE 1.0.0", 750, 40);
    ofDrawBitmapString("SENSORS", 750, 80);
    ofDrawBitmapString("------------------------------", 750, 100);
    int textBlock = 0;
    int ypos = 120;
    int itemCount = sensorReceiver.sensors.size();
    int colLength = ceil(itemCount/2);
    for (int i = 0; i < itemCount; i += 1)
    {
        if (i == colLength)
        {
            textBlock += 1;
        }
        ofDrawBitmapString(ofToString(i+1, 2, '0') + ": " + (sensorReceiver.sensors[i].currentValue > 0.0 ? "ON" : "OFF"), 750 + textBlock*128, ypos + (i-textBlock*colLength)*20);
    }
    
    ofDrawBitmapString("RELAYS", 750, 280);
    ofDrawBitmapString("------------------------------", 750, 300);
    textBlock = 0;
    ypos = 320;
    itemCount = relayManager.relays.size();
    colLength = ceil(itemCount/2);
    for (int i = 0; i < itemCount; i += 1)
    {
        if (i == colLength)
        {
            textBlock += 1;
        }
        ofDrawBitmapString(ofToString(i+1, 2, '0') + ": " + (relayManager.relays[i].getRelayState() > 0 ? "ON" : "OFF"), 750 + textBlock*128, ypos + (i-textBlock*colLength)*20);
    }
    
    ofDrawBitmapString("DMX CHANNELS", 750, 420);
    ofDrawBitmapString("------------------------------", 750, 440);
    textBlock = 0;
    ypos = 460;
    int dmxFix = 0;
    for (int i = 0; i < 14; i += 1)
    {
        if (i == 3)
        {
            dmxFix += 1;
        }
        if (i == 7)
        {
            textBlock += 1;
        }
        int dmxPos = i + dmxFix;
        int dmxValue = dmxControl.getChannel(dmxPos);
        ofSetColor(dmxValue, dmxValue, dmxValue);
        ofFill();
        ofRect(750 + textBlock*128, ypos + (i-textBlock*7)*20-10, 30, 15);
        ofNoFill();
        ofSetColor(255, 255, 255);
        ofDrawBitmapString(ofToString(i+1, 2, '0') + ": " + ofToString(float(dmxValue)/255.0*100.0, 2, 6, '0') + "%", 750 + textBlock*128 + 35, ypos + (i-textBlock*7)*20+2);
    }
    
    gol.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    switch (key)
    {
        case ' ':
            mainGui->setVisible(!mainGui->isVisible());
            break;
        case 'f':
            ofToggleFullscreen();
            break;
        case 'r':
            for (int i = 0; i < 14; i += 1)
            {
                int xOffset = 0;
                if (i > 6)
                {
                    xOffset = 1;
                }
                gol.addPufferTrain(xOffset*35+7, int(70/7*(i-xOffset*7)+1));
            }
            break;
        case 'p':
            int sensorId = rand() % 13 + 1;
            int xOffset = 0;
            if (sensorId-7 >= 0)
            {
                xOffset = 1;
            }
            gol.addPufferTrain(xOffset*35+7, int(70/7*(sensorId-xOffset*7)+1));
            break;
    }
}

void ofApp::setupMainGUI()
{
    mainGui = new ofxUICanvas();
    mainGui->setVisible(false);
    mainGui->setWidth(300.0);
    mainGui->setHeight(ofGetHeight());
    mainGui->setColorBack(ofxUIColor(200,200,200,240));
    mainGui->setColorFill(ofxUIColor(30,30,30,255));
    mainGui->setColorFillHighlight(ofxUIColor(80,0,0,255));
    mainGui->setColorOutline(ofxUIColor(30,30,30,255));
    mainGui->setColorOutlineHighlight(ofxUIColor(80,0,0,255));
    
    mainGui->addLabel("SETTINGS");
    
    mainGui->addSpacer();
    
    mainGui->addToggle("USE CA", _useCA);
    mainGui->addToggle("USE RANDOM", _useRandom);
    mainGui->addSlider("RANDOM SCALE", 0.0, 1.0, _randomScale);
    
    mainGui->addSpacer();
    
    mainGui->addToggle("USE LOW PAD", _useLowPad);
    mainGui->addIntSlider("LOW PAD", 1, 255, _lowPadPercent);
    
    mainGui->addSpacer();
    
    mainGui->addToggle("USE HIGH PASS", _useHighPass);
    mainGui->addIntSlider("HIGH PASS", 1, 255, _highPassPercent);
    
    ofAddListener(mainGui->newGUIEvent, this, &ofApp::guiEvent);
    
    mainGui->loadSettings("settings.xml");
}

void ofApp::exit()
{
    mainGui->saveSettings("settings.xml");
    delete mainGui;
}

void ofApp::guiEvent(ofxUIEventArgs &e)
{
    if (e.getName() == "USE CA")
    {
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        _useCA = toggle->getValue();
    }
    else if (e.getName() == "USE RANDOM")
    {
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        _useRandom = toggle->getValue();
    }
    else if (e.getName() == "RANDOM SCALE")
    {
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        _randomScale = slider->getValue();
    }
    else if (e.getName() == "USE LOW PAD")
    {
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        _useLowPad = toggle->getValue();
    }
    else if (e.getName() == "LOW PAD")
    {
        ofxUIIntSlider *slider = (ofxUIIntSlider *) e.widget;
        _lowPadPercent = slider->getValue();
    }
    else if (e.getName() == "USE HIGH PASS")
    {
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        _useHighPass = toggle->getValue();
    }
    else if (e.getName() == "HIGH PASS")
    {
        ofxUIIntSlider *slider = (ofxUIIntSlider *) e.widget;
        _highPassPercent = slider->getValue();
    }
}

