//
//  patterns.h
//  conway
//
//  Created by Chris Roby on 6/2/12.
//  Copyright (c) 2012 Chris Roby, 70bpm, LLC. All rights reserved.
//

#pragma once

#include "GOLGrid.h"

class GOLPatterns
{
    public:
        static void gliderGun(GOLCell **grid, int startPosX, int startPosY);
        static void pufferTrain(GOLCell **grid, int startPosX, int startPosY);
};