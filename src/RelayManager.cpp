//
//  RelayManager.cpp
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "RelayManager.h"
#include "OscDataSender.h"

void RelayManager::init(OscDataSender &oscDataSender)
{
    _dataSender = &oscDataSender;
    relays.clear();
}

void RelayManager::addRelay(std::string host, int port, int relayIndex)
{
    RelayControl relay;
    relay.init(_dataSender, host, port, relayIndex);
    relays.push_back(relay);
}

void RelayManager::setRelayByIndex(int relayIndex, int state)
{
    for (int i = 0; i < relays.size(); i += 1)
    {
        if (relays[i].index == relayIndex)
        {
            relays[i].setRelayState(state);
        }
    }
}

int RelayManager::getRelayByIndex(int relayIndex)
{
    int state = 0;
    for (int i = 0; i < relays.size(); i += 1)
    {
        if (relays[i].index == relayIndex)
        {
            state = relays[i].getRelayState();
        }
    }
    return state;
}