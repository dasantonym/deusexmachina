#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxUI.h"
#include "ofEvents.h"
#include "GOLGrid.h"
#include "GOLSampler.h"
#include "EnttecDmxControl.h"
#include "SensorOscReceiver.h"
#include "ArduinoLedControl.h"
#include "RelayManager.h"
#include "OscDataSender.h"
#include "RandomList.h"

#define RANDOM_LIST_LENGTH 10000
#define OSC_SAMPLE_OUT_PORT 9000
#define OSC_SAMPLE_OUT_IP "127.0.0.1"

struct relay
{
    bool state;
    int frames;
    std::string ip;
    int portId;
    int relayIndex;
};

struct neonlight
{
    int value;
    int frames;
    int portId;
};

class ofApp : public ofBaseApp
{

	public:
		void setup();
		void update();
		void draw();
        
        void setupMainGUI();
        void exit();
        void guiEvent(ofxUIEventArgs &e);
		void keyPressed(int key);
    
        GOLGrid gol;
        GOLSampler golSampler;
        EnttecDmxControl dmxControl;
        SensorOscReceiver sensorReceiver;
        ArduinoLedControl ledControl;
        RelayManager relayManager;
        OscDataSender dataSender;
        RandomList randomList;
    
        int rotator;
    
        ofxUICanvas* mainGui;
    
    private:
        bool _useCA;
        bool _useRandom;
        float _randomScale;
        bool _useLowPad;
        int _lowPadPercent;
        bool _useHighPass;
        int _highPassPercent;
        int _sampleX;
        int _sampleY;
        int _sampleTicks;
        int _golTicks;
        int _numSensors;
};
