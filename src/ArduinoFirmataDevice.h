//
//  ArduinoFirmataDevice.h
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "ofMain.h"

class ArduinoFirmataDevice
{
public:
    void open(std::string serialPath, int baudRate);
    void update();
    void setAnalogPinOut(int pin, int value);
    
private:
    void setupArduino(const int &version);
    
    ofArduino _device;
    ofSerial _serial;
    bool _isReady;
};
