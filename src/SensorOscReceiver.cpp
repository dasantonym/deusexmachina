//
//  SensorOscReceiver.cpp
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "SensorOscReceiver.h"

void SensorOscReceiver::init(int numSensors)
{
    ofLogNotice("SensorOscReceiver::init") << "Listening on port: " << OSC_INPUT_PORT;
    _oscReciever.setup(OSC_INPUT_PORT);
    
    _numSensors = numSensors;
    
    for (int i = 0; i < _numSensors; i += 1)
    {
        Sensor sensor;
        sensor.currentValue = 0.0;
        sensor.lastValue = 0.0;
        sensors.push_back(sensor);
    }
}

void SensorOscReceiver::update()
{
    for (int i = 0; i < _numSensors; i += 1)
    {
        sensors[i].lastValue = sensors[i].currentValue;
    }
    while (_oscReciever.hasWaitingMessages())
    {
        ofxOscMessage msg;
        _oscReciever.getNextMessage(&msg);
        if (msg.getAddress() == "/sensor")
        {
            int sensorIndex = msg.getArgAsInt32(0) - 1; // TODO: make this 0 based everywhere
            float sensorValue = msg.getArgAsFloat(1);
            sensors[sensorIndex].currentValue = sensorValue;
        }
    }
}

bool SensorOscReceiver::isSensorValueChanged(int sensorIndex)
{
    return sensors[sensorIndex].lastValue != sensors[sensorIndex].currentValue;
}

float SensorOscReceiver::getSensorValue(int sensorIndex)
{
    return sensors[sensorIndex].currentValue;
}

