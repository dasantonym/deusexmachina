//
//  GOLSampler.h
//  DeusExMachina
//
//  Created by Anton on 16/11/14.
//
//

#include <stdio.h>
#include <vector>
#include "GOLGrid.h"

class GOLSampler
{
public:
    void init(int samplesX, int samplesY, GOLGrid* grid);
    void update();
    
    vector<float> sampleGrid;
    
private:
    GOLGrid* _grid;
    std::vector<float> _values;
    int _samplesX;
    int _samplesY;
};