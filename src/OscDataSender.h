//
//  OscDataSender.h
//  DeusExMachina
//
//  Created by Anton on 16/11/14.
//
//

#include "ofMain.h"
#include "ofxOsc.h"

class OscDataSender
{
public:
    void sendMessage(std::string host, int port, std::string address, vector<float> floatArgs);
    
private:
    ofxOscSender _oscSender;
};