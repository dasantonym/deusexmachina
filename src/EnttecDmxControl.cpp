//
//  EnttecDmxControl.cpp
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "EnttecDmxControl.h"

void EnttecDmxControl::init()
{
    memset( _dmxData, 0, DMX_DATA_LENGTH );
    
    _dmxInterface = ofxGenericDmx::openFirstDevice();
    
    if ( _dmxInterface == 0 )
    {
        ofLogNotice("ofApp::setup") << "no enttec device";
    }
    else
    {
        ofLogNotice("ofApp::setup") << "enttec open" << _dmxInterface->isOpen();
    }
    
    ofLogNotice("ofApp::setup") << "dmx addon version: " << ofxGenericDmx::VERSION_MAJOR << "." << ofxGenericDmx::VERSION_MINOR;
}

void EnttecDmxControl::setChannel(int channel, int value)
{
    _dmxData[channel + 1] = value;
}

int EnttecDmxControl::getChannel(int channel)
{
    return _dmxData[channel + 1];
}

void EnttecDmxControl::sendDmxData()
{
    _dmxData[0] = 0;
    if (_dmxInterface && _dmxInterface->isOpen())
    {
        _dmxInterface->writeDmx( _dmxData, DMX_DATA_LENGTH );
    }
}