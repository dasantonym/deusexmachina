//
//  EnttecDmxControl.h
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "ofMain.h"
#include "ofxGenericDmx.h"

#define DMX_DATA_LENGTH 513

class EnttecDmxControl
{
public:
    void init();
    void setChannel(int channel, int value);
    void sendDmxData();

    int getChannel(int channel);

private:
    DmxDevice* _dmxInterface;
    unsigned char _dmxData[DMX_DATA_LENGTH];
};