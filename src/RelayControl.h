//
//  RelayOscSender.h
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "ofMain.h"

class OscDataSender;

struct Relay
{
    float currentState;
    float lastState;
};

class RelayControl
{
public:
    void init(OscDataSender* oscSender, std::string host, int port, int relayIndex);
    
    void setRelayState(int state);
    
    int getRelayState();
    
    int index;

private:
    OscDataSender* _oscSender;
    int _relayState;
    std::string _host;
    int _port;
};