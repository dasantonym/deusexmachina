//
//  RelayManager.h
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "RelayControl.h"

class OscDataSender;

class RelayManager
{
public:
    void init(OscDataSender &oscDataSender);
    void addRelay(std::string host, int port, int relayIndex);
    void setRelayByIndex(int relayIndex, int state);
    
    int getRelayByIndex(int relayIndex);
    
    vector<RelayControl> relays;
private:
    OscDataSender* _dataSender;
};