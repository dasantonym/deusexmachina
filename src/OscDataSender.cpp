//
//  OscDataSender.cpp
//  DeusExMachina
//
//  Created by Anton on 16/11/14.
//
//

#include "OscDataSender.h"

void OscDataSender::sendMessage(std::string host, int port, std::string address, vector<float> floatArgs)
{
    _oscSender.setup(host, port);
    ofxOscMessage msg;
    msg.setRemoteEndpoint(host, port);
    msg.setAddress(address);
    for (std::vector<float>::iterator it = floatArgs.begin(); it < floatArgs.end(); it += 1)
    {
        msg.addFloatArg(floatArgs[*it]);
    }
    _oscSender.sendMessage(msg);
}
