//
//  conway.h
//  DeusExMachina
//
//  Created by Anton on 05/11/14.
//
//

#pragma once

#include "ofMain.h"

struct GOLCell
{
    bool currState;
    bool nextState;
    int ticks;
    int activeNeighbors;
};

class GOLGrid
{
    public:
        void init(int width, int height, int cellsize, int xOffset, int yOffset, int tickInterval);

        void tick();
        void update();
        void draw();
        void clear();

        void addGliderGun(int x, int y);
        void addPufferTrain(int x, int y);

        GOLCell **grid;
        int rows, cols;

    private:
        int _xOffset, _yOffset, _width, _height, _cellSize, _tickInterval;
        float _cellWidth, _cellHeight;
        bool _active;

        int getNumActiveNeighbors(int colIndex, int rowIndex);
        int currState(int colIndex, int rowIndex);
    
        void makeNextStateCurrent();
};
