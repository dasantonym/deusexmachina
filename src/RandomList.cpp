//
//  RandomList.cpp
//  DeusExMachina
//
//  Created by Anton on 15/11/14.
//
//

#include "RandomList.h"

void RandomList::generate(int size)
{
    _values.clear();
    _pointer = 0;
    _size = size;
    for (int i = 0; i < size; i += 1)
    {
        _values.push_back(float(rand() % 100000)/100000);
    }
}

float RandomList::getValue()
{
    float value = _values[_pointer];
    _pointer += 1;
    if (_pointer == _size)
    {
        _pointer = 0;
    }
    return value;
}