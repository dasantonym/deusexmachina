# DeusExMachina #

Control software for the "Geisterfahrer" installation. Mac/Linux only because the DMX addon won't work on windows (AFAIK).

### How to build ###

* You need a working install of [OpenFrameworks 0.8.4](http://openframeworks.cc/download/)
* Get the addons [ofxGenericDmx](https://github.com/dasantonym/ofxGenericDmx) (don't forget to compile the libs on linux!) and [ofxUI](https://github.com/rezaali/ofxUI).

```
#!bash

git clone https://dasantonym@bitbucket.org/dasantonym/DeusExMachina.git
cd DeusExMachina
make
make run
```
